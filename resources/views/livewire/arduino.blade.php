<div>
    <div class="max-w-6xl mx-auto sm:px-8 lg:px-8">
        <canvas id="myChart" width="200" height="200"></canvas>
    </div>

    @push('js')
        <script>
            setInterval(() => Livewire.emit('ubahData'), 1000);
            var chartData = JSON.parse(`<?php echo $arduino; ?>`);
            console.log(chartData);
            const ctx = document.getElementById('myChart').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: chartData.time,
                    datasets: [{
                        label: 'Jarak',
                        data: chartData.meter,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            Livewire.on('berhasilUpdate', event => {
                var chartData = JSON.parse(event.data);
                console.log(chartData);
                myChart.data.labels = chartData.time;
                myChart.data.datasets.forEach((dataset) => {
                    dataset.data = chartData.meter;
                });
                myChart.update();
            })
        </script>
    @endpush
</div>