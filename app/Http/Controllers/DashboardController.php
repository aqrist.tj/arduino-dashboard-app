<?php

namespace App\Http\Controllers;

use App\Models\Arduino;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function mount()
    {
        $arduino = Arduino::latest()->limit(10)->get();
        foreach ($arduino as $item) {
            $data['jarak'][] = (int)$item->jarak;
            $data['meter'][] = (int)$item->meter;
        }
        $this->arduino = json_encode($data);
        dd($this->arduino);
    }

    public function dashboard()
    {
        $arduino = Arduino::latest()->limit(10)->get();
        foreach ($arduino as $item) {
            $data['jarak'][] = (int)$item->jarak;
            $data['meter'][] = (int)$item->meter;
        }
        $exact = json_encode($data);

        return view('dashboard', ['arduino' => $exact]);
    }

    public function changeData()
    {
        $arduino = Arduino::latest()->limit(10)->get();
        foreach ($arduino as $item) {
            $data['jarak'][] = (int)$item->jarak;
            $data['meter'][] = (int)$item->meter;
        }
        $exact = json_encode($data);
    }
}
