<?php

namespace App\Http\Controllers;

use App\Models\Arduino;
use Illuminate\Http\Request;

class ArduinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arduino = Arduino::latest()->limit(10)->get();
        return view('dashboard', ['arduino' => $arduino]);
    }

    public function mount()
    {
        $arduino = Arduino::latest()->limit(10)->get();
        foreach ($arduino as $item) {
            $data['jarak'][] = $item->jarak;
            $data['meter'][] = $item->meter;
        }
        $this->arduino = json_encode($data);
        // dd($this->arduino);
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Arduino  $arduino
     * @return \Illuminate\Http\Response
     */
    public function show(Arduino $arduino)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Arduino  $arduino
     * @return \Illuminate\Http\Response
     */
    public function edit(Arduino $arduino)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Arduino  $arduino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Arduino $arduino)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Arduino  $arduino
     * @return \Illuminate\Http\Response
     */
    public function destroy(Arduino $arduino)
    {
        //
    }
}
