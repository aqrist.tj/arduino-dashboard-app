<?php

namespace App\Http\Controllers\API;

use App\Models\Arduino;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

class ArduinoController extends Controller
{
    public function storeindb(Request $request)
    {
        $jarak = $request->input('jarak');
        $meter = $request->input('meter');

        $transaction  = Arduino::create([
            'jarak' => $jarak,
            'meter' => $meter,
        ]);

        if ($transaction) {
            return ResponseFormatter::success(
                $transaction,
                'transaksi berhasil'
            );
        } else {
            return ResponseFormatter::error(
                'error'
            );
        }
    }
}
