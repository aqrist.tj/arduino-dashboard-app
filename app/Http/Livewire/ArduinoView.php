<?php

namespace App\Http\Livewire;

use App\Models\Arduino;
use Livewire\Component;

class ArduinoView extends Component
{
   public $arduino;
   protected $listeners = ['ubahData' => 'changeData'];
   public function mount()
   {
      $arduino = Arduino::latest()->limit(10)->get();
      foreach ($arduino as $item) {
         $data['time'][] = $item->created_at->format('H:i:s');
         $data['meter'][] = (int)$item->meter;
      }
      $this->arduino = json_encode($data);
   }

   public function render()
   {
      return view('livewire.arduino')->extends('layouts.master')->section('content');
   }

   public function changeData()
   {
      $arduino = Arduino::latest()->limit(10)->get();
      foreach ($arduino as $item) {
         $data['time'][] = $item->created_at->format('H:i:s');
         $data['meter'][] = (int)$item->meter;
      }
      $this->arduino = json_encode($data);
      $this->emit('berhasilUpdate', ['data' => $this->arduino = json_encode($data)]);
   }
}
